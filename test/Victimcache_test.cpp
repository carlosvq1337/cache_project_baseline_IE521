/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include "debug_utilities.h"
#include "utilities.h"
#include "L1cache.h"
#include "Victimcache.h"


using namespace std;

class VCcache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

 /*
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 4. Se inserta A en el sistema de caches
 * 5. Se victimiza A de L1
 * 6. Se genera un miss en L1 y un hit en VC al accesar A
  */
TEST_F(VCcache,l1_miss_vc_hit){
	DEBUG(debug_on, l1_miss_vc_hit);
 	int status = OK;
 	operation_result l1_result = {};
 	operation_result vc_result = {};

 	// Se genera una configuración aleatoria de cache
 	parameters params = random_params();
	params.opt = VC;
 	// Se calculan los sizes
 	sizes sizes = get_sizes(params);

 	// Se genera un acceso aleatorio
 	line_info access = random_access();
	bool loadstore = rand()%2;

	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, VC);
		printf("** The original memory access for block A: 0x%X **\n", access.address);
	}

 	// Se genera un entry_info a partir de access
 	entry_info A = get_entry_info(access.address,sizes);

 	// Se crean L1 y VC
 	entry l1_cache_blocks[sizes.l1_assoc] = {};
 	entry vc_cache_blocks[sizes.vc_assoc] = {};

	// Print state
	if(debug_on){
		printf("\n** Empty L1 and VC **\n");
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

 	// Se inserta A
 	status = lru_replacement_policy_l1_vc(&A,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,debug_on);
 	EXPECT_EQ(status,OK);

	// Print state
	if(debug_on){
		printf("\n** Block A has been inserted in L1 **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

 	// Se llena L1 hasta victimizar A, lo que deberia insertarlo en VC.
 	for (size_t i = 0; i < sizes.l1_assoc; i++){
		line_info  access = random_access();
		entry_info B = get_entry_info(access.address,sizes);
		while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_vc(&B,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,false);
		EXPECT_EQ(status,OK);
 	}

	// Print state
	if(debug_on){
		printf("\n** Block A has been victimized from L1 and inserted in VC **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

 	// Se referencia a A, lo que deberia provocar miss en L1 pero hit en VC.
 	status = lru_replacement_policy_l1_vc(&A,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,debug_on);
 	EXPECT_EQ(status, OK);

	// Comprobación de miss en L1
	int expected_l1 = (loadstore) ? MISS_STORE : MISS_LOAD;
 	// Comprobación de miss en l1
 	EXPECT_EQ(l1_result.miss_hit,expected_l1);

	// Comprobación de hit en VC
	int expected_vc = (loadstore) ? HIT_STORE : HIT_LOAD;
 	EXPECT_EQ(vc_result.miss_hit,expected_vc);
 }

 /*
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 4. Se inserta A en el sistema de caches
 * 5. Se victimiza A de L1 y VC
 * 6. Se genera un miss en L1 y VC al accesar A
  */
 TEST_F(VCcache,l1_miss_vc_miss){
	 DEBUG(debug_on, l1_miss_vc_miss);
	 int status = OK;
	 operation_result l1_result = {};
	 operation_result vc_result = {};

	 // Se genera una configuración aleatoria de cache
	 parameters params = random_params();
	 params.opt = VC;
	 // Se calculan los sizes
	 sizes sizes = get_sizes(params);

	 // Se genera un acceso aleatorio
	 line_info access = random_access();
	 bool loadstore = rand()%2;

	 // Print test params:
	 if(debug_on){
		 printParams(params);
		 print_sizes(sizes, VC);
		 printf("** The original memory access for block A: 0x%X **\n", access.address);
	 }

 	// Se genera un entry_info a partir de access
 	entry_info A = get_entry_info(access.address,sizes);

 	// Se crean L1 y VC
 	entry l1_cache_blocks[sizes.l1_assoc] = {};
 	entry vc_cache_blocks[sizes.vc_assoc] = {};

	// Print state
	if(debug_on){
		printf("\n** Empty L1 and VC **\n");
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

 	// Se inserta A
 	status = lru_replacement_policy_l1_vc(&A,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,debug_on);
 	EXPECT_EQ(status,OK);

	// Print state
	if(debug_on){
		printf("\n** Block A has been inserted in L1 **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

 	// Se llena L1 hasta victimizar A tanto de L1 como de L2
 	for (size_t i = 0; i < (sizes.l1_assoc + sizes.vc_assoc); i++){
		line_info  access = random_access();
		entry_info B = get_entry_info(access.address,sizes);
		while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag) || is_in_set(vc_cache_blocks,B.vc_assoc,B.l1_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_vc(&B,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,false);
		EXPECT_EQ(status,OK);
 	}

	// Print state
	if(debug_on){
		printf("\n** Block A has been victimized from L1 and VC **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

 	// Se referencia a A
 	status = lru_replacement_policy_l1_vc(&A,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,debug_on);
 	EXPECT_EQ(status, OK);

	// Comprobación de miss en L1
	int expected_l1 = (loadstore) ? MISS_STORE : MISS_LOAD;
 	// Comprobación de miss en l1
 	EXPECT_EQ(l1_result.miss_hit,expected_l1);

	// Comprobación de miss en VC
	int expected_vc = (loadstore) ? MISS_STORE : MISS_LOAD;
 	EXPECT_EQ(vc_result.miss_hit,expected_vc);
 }



/* NEW TESTS*/


/* 
 * TEST3:
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 4. Se inserta A en el sistema de caches
 * 5. Se victimiza A de L1
 * 6. Se genera un miss en L1 y un hit en VC al accesar A
 * 7. Se verifica que se dé el swap entre los bloques correspondientes en L1 y VC
  */
TEST_F(VCcache,l1_vc_swap){
	DEBUG(debug_on, l1_vc_swap);
 	int status = OK;
 	operation_result l1_result = {};
 	operation_result vc_result = {};

 	// Se genera una configuración aleatoria de cache
 	parameters params = random_params();
	params.opt = VC;
 	// Se calculan los sizes
 	sizes sizes = get_sizes(params);

 	// Se genera un acceso aleatorio
 	line_info access = random_access();
	bool loadstore = rand()%2;

	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, VC);
		printf("** The original memory access for block A: 0x%X **\n", access.address);
	}

 	// Se genera un entry_info a partir de access
 	entry_info A = get_entry_info(access.address,sizes);

 	// Se crean L1 y VC
 	entry l1_cache_blocks[sizes.l1_assoc] = {};
 	entry vc_cache_blocks[sizes.vc_assoc] = {};

	// Print state
	if(debug_on){
		printf("\n** Empty L1 and VC **\n");
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

 	// Se inserta A
 	status = lru_replacement_policy_l1_vc(&A,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,debug_on);
 	EXPECT_EQ(status,OK);

	// Print state
	if(debug_on){
		printf("\n** Block A has been inserted in L1 **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

 	// Se llena L1 hasta victimizar A, lo que deberia insertarlo en VC.
 	for (size_t i = 0; i < sizes.l1_assoc; i++){
		line_info  access = random_access();
		entry_info B = get_entry_info(access.address,sizes);
		while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_vc(&B,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,false);
		EXPECT_EQ(status,OK);
 	}

	// Print state
	if(debug_on){
		printf("\n** Block A has been victimized from L1 and inserted in VC **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

 	// Se referencia a A, lo que deberia provocar miss en L1 pero hit en VC.
 	status = lru_replacement_policy_l1_vc(&A,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,debug_on);
 	EXPECT_EQ(status, OK);

	/* Check if blocks were swapped between L1 and VC */

	/* Tag used to look for block in l1, should be block A, which was originally a miss in L1*/
	int expected_tag_l1 = A.l1_tag;
	bool found_l1 = false;

	/* Tag used to look for block in vc, should be the recently evicted block */
	int expected_tag_vc = l1_result.evicted_address;
	bool found_vc = false;

	for (size_t i = 0; i < sizes.l1_assoc; i++)
	{
		if (l1_cache_blocks[i].tag == expected_tag_l1)
		{
			found_l1 = true;	
		}
	}

	for (size_t i = 0; i < sizes.vc_assoc; i++)
	{
		if (vc_cache_blocks[i].tag == expected_tag_vc)
		{
			found_vc = true;	
		}
	}
	/* Both blocks are expected to be found in respective caches to confirm swap */
	EXPECT_EQ(found_l1,true);
 	EXPECT_EQ(found_vc,true);
 }

 /* 
 * TEST4:
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A **siempre STORES
 * 4. Se inserta A en el sistema de caches
 * 5. Se generan suficientes accesos load aleatorios para que A sea victimizado de VC (luego de ser victimizado de L1)
 * 6. Se reinserta A, lo que lleva a la victimización de uno de los accesos aleatorios load
 * 7. Se verifica el dirty eviction de A (store, dirty eviction = 1) y el último bloque victimizado (load, dirty eviction = 0)
  */
TEST_F(VCcache,vc_eviction){
	DEBUG(debug_on, vc_eviction);
 	int status = OK;
 	operation_result l1_result = {};
 	operation_result vc_result = {};

 	// Se genera una configuración aleatoria de cache
 	parameters params = random_params();
	params.opt = VC;

 	// Se calculan los sizes
 	sizes sizes = get_sizes(params);

	/* A random acces is generated, but making sure it is always a store */
 	line_info access = random_access();
	bool loadstore = 1;

	// Se crean L1 y VC
 	entry l1_cache_blocks[sizes.l1_assoc] = {};
 	entry vc_cache_blocks[sizes.vc_assoc] = {};

	// Print state
	if(debug_on){
		printf("\n** Empty L1 and VC **\n");
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}	 



	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, VC);
		printf("** The original memory access for block A: 0x%X **\n", access.address);
	}

 	// Se genera un entry_info a partir de access
 	entry_info A = get_entry_info(access.address,sizes);

 	// Se inserta A
 	status = lru_replacement_policy_l1_vc(&A,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,debug_on);
 	EXPECT_EQ(status,OK);

	// Print state
	if(debug_on){
		printf("\n** Block A has been inserted in L1 **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

	/* Enough loads are executed to make sure A was victimized from both caches */
 	for (size_t i = 0; i < (sizes.l1_assoc + sizes.vc_assoc); i++){
		line_info  access = random_access();
		bool loadstore = 0;
		entry_info B = get_entry_info(access.address,sizes);
		while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag) || is_in_set(vc_cache_blocks,B.vc_assoc,B.l1_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_vc(&B,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,false);
		EXPECT_EQ(status,OK);
 	}

	// Print state
	if(debug_on){
		printf("\n** Block A has been victimized from L1 and VC **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

	/* Dirty eviction value for A is stored */
	bool dirty_A = vc_result.dirty_eviction;

	/* A is reinserted into L1, which evicts one of the loaded blocks */
	status = lru_replacement_policy_l1_vc(&A,loadstore,l1_cache_blocks,vc_cache_blocks,&l1_result,&vc_result,debug_on);
 	EXPECT_EQ(status,OK);

	// Print state
	if(debug_on){
		printf("\n** Block A has been inserted in L1 **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(vc_cache_blocks, sizes.vc_assoc,"VC |");
	}

	/* Dirty bit for the recently evicted block is stored */
	bool dirty_B = vc_result.dirty_eviction;

	/* A is expected to be a dirty eviction, since it was a store */
 	EXPECT_EQ(dirty_A,true);
	/* This was a load access, so it is expeced to be a "clean" eviction */
	EXPECT_EQ(dirty_B,false);
}