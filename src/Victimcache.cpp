/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
    	                      	bool loadstore,
       	                    	entry* l1_cache_blocks,
       	                  	 	entry* vc_cache_blocks,
        	                 	operation_result* l1_result,
              	            	operation_result* vc_result,
                	         	bool debug)
{

	/* For checking hits, initialized as false */ 
	bool hit_l1 = false;
	bool hit_vc = false;

	/* to keep VC from "working" if L1 is not full, initialized as false */
	bool active_vc = false;

	/* Check if L1 is full */
	int count_valids_active = 0;
	for (size_t i = 0; i < l1_vc_info->l1_assoc; i++)
	{
		if (l1_cache_blocks[i].valid == true)
		{
			count_valids_active++;	
		}
	}
	/* If valid entries == L1 size -> L1 is full -> Activate VC */
	if (count_valids_active == l1_vc_info->l1_assoc)
	{
		active_vc = true;
	}	
		

	/*Check for hit in L1*/	
	for (int i = 0; i < l1_vc_info->l1_assoc; i++)
	{
		if (l1_vc_info->l1_tag == l1_cache_blocks[i].tag && hit_l1 == false)
		{
			/* If tag value is found, there is a hit in L1 */
			hit_l1 = true;
		}
	}

	/*Check for hit in VC*/
	for (int i = 0; i < l1_vc_info->vc_assoc; i++)
	{
		if (l1_vc_info->l1_tag == vc_cache_blocks[i].tag && hit_vc == false)
		{
			/* If tag value is found, there is a hit in VC */
			hit_vc = true;
		}
	}

	/* In case of hit in L1, simply execute LRU on L1 as usual*/
	if(hit_l1){
		lru_replacement_policy (l1_vc_info->l1_idx,
                             l1_vc_info->l1_tag,
                             l1_vc_info->l1_assoc,
                             loadstore,
                             l1_cache_blocks,
                             l1_result,
                             debug);
		
	}

	/* In case of miss in L1 but hit in VC, LRU is ran on L1 to get evicted adress result*/
	if(hit_l1 == false && hit_vc == true){
		lru_replacement_policy (l1_vc_info->l1_idx,
                             l1_vc_info->l1_tag,
                             l1_vc_info->l1_assoc,
                             loadstore,
                             l1_cache_blocks,
                             l1_result,
                             debug);

		/* Swap blocks between L1 and VC */
		for (int i = 0; i < l1_vc_info->vc_assoc; i++)
		{
			if (l1_vc_info->l1_tag == vc_cache_blocks[i].tag)
			{
				/* corresponding tag on VC is set to evicted adress value from L1 */
				vc_cache_blocks[i].tag = l1_result->evicted_address;
			}
		}
		
		/* if LOAD */
        if(loadstore == 0){
        	vc_result->miss_hit = HIT_LOAD;
        }

        /* if STORE */
		else{
        	vc_result->miss_hit = HIT_STORE;
        }
	}

	/* In case of miss on L1 and VC*/
	if(hit_l1 == false && hit_vc == false){
		lru_replacement_policy (l1_vc_info->l1_idx,
                             l1_vc_info->l1_tag,
                             l1_vc_info->l1_assoc,
                             loadstore,
                             l1_cache_blocks,
                             l1_result,
                             debug);

		/* Set dirty eviction true if dirty bit of last block (to be evicted) is 1*/
		if(vc_cache_blocks[l1_vc_info->vc_assoc-1].dirty == true){
           vc_result->dirty_eviction = true;
        }
        else{
           vc_result->dirty_eviction = false;
        }

		/*Evicted address set as tag of last (rightmost) block*/
		vc_result->evicted_address = vc_cache_blocks[l1_vc_info->vc_assoc-1].tag;

        /* if LOAD */
        if(!loadstore){
			/* No need to set dirty bit in LOAD scenarios */
            vc_cache_blocks[l1_vc_info->vc_assoc-1].dirty = false;
            vc_result->miss_hit = MISS_LOAD;
        }

        /* if STORE */
        else{
			/* Dirty bit is set in STORE scenarios */
            vc_cache_blocks[l1_vc_info->vc_assoc-1].dirty = true;
            vc_result->miss_hit = MISS_STORE;
        }

		/* If there were no hits in either cache, and VC is active, FIFO must be shifted to accomodate evicted block from L1*/
		if (active_vc == true)
		{
			/* Shifting: */					
			int temp = vc_cache_blocks[0].tag;
			bool temp_valid = vc_cache_blocks[0].valid;
			bool temp_dirty = vc_cache_blocks[0].dirty;
			for (int i = 0; i < l1_vc_info->vc_assoc-1; i++)
			{	
				int aux = vc_cache_blocks[i+1].tag;
				bool aux_valid = vc_cache_blocks[i+1].valid;
				bool aux_dirty = vc_cache_blocks[i+1].dirty;
				vc_cache_blocks[i+1].tag = temp;
				vc_cache_blocks[i+1].valid = temp_valid;
				vc_cache_blocks[i+1].dirty = temp_dirty;
				temp = aux;
				temp_valid = aux_valid;
				temp_dirty = aux_dirty;
			}

			/* First block in VC is now the most recently evicted L1 block */
			vc_cache_blocks[0].tag = temp;
			vc_cache_blocks[0].valid = temp_valid;
			vc_cache_blocks[0].dirty = temp_dirty;
			vc_cache_blocks[0].tag = l1_result->evicted_address;

			/*Updates valid and dirty bit in VC*/
			int count_valids = 0;
			for (size_t i = 0; i < l1_vc_info->l1_assoc; i++)
			{
				if (l1_cache_blocks[i].valid == true)
				{
					count_valids++;	
				}
			}
			if (count_valids == l1_vc_info->l1_assoc)
			{
				vc_cache_blocks[0].valid = true;
			}
			else
			{
				vc_cache_blocks[0].valid = false;
			}
			if (l1_result->dirty_eviction)
			{
				vc_cache_blocks[0].dirty = true;
			}
			else
			{
				vc_cache_blocks[0].dirty = false;
			}
			
			
		}
		
	}
   	
   	return OK;
}